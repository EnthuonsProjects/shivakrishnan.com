<?php $url = 'http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$parts = parse_url($url);
$path = $parts['path'];
$keys = explode('/', $path);
//$key= $keys[2];
//echo $key;exit;
?>

<header>
  <?php require('blog/wp-blog-header.php'); ?>
  <div class="container_12">
    <div class="row">
      <div class="grid_12 clearfix">
        <div class="clearfix fleft sn_logo">
          <h1><a href="index.php"><img src="images/logo.png" alt="shiva krishnan" title="shiva krishnan" /></a></h1>
          <div class="tagLine">‘Manifestation is not about creating an item or a situation in your life. It’s entirely about becoming the vibration at which what you desire already exists.’
            <div class="right_go"> - Shiva Krishnan </div>
          </div>
        </div>
        <nav class="clearfix">
          <form id="search1" class="search" action="<?php bloginfo('url'); ?>/" method="GET">
            <div>
              <input type="text" name="s">
              <a onClick="document.getElementById('search1').submit()" class="button1"><i class="icon-search"></i></a> </div>
          </form>
          <ul class="sf-menu clearfix">
            <li><a href="index.php">Home</a></li>
            <li <?php if($keys[1]=="index-1.php"){?> class="current" <?php }?>><a href="index-1.php">About us</a></li>
            <li><a href="blog/index.php" >Blog</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>
