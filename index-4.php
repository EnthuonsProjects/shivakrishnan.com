<?php
if (!empty($_POST['sender_email']) && $_POST['sender_email'] != '') {

	$name = $_POST['sender_name'];
	$from = $_POST['sender_email'];
	$phone = $_POST['sender_phone'];
	$message = $_POST['sender_message'];
	$message = wordwrap($message, 70);
	$subject = $name . " query. Phone number: " . $phone; 
	mail("shiva.krishnan@optusnet.com.au",$subject,$message,"From: $from\n");
	//echo "Thank you for sending us feedback";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Contact Us</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="css/contacts.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script src="js/TMForm.js"></script>
    <script>
        $(document).ready(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $('#contact-form').TMForm({
                ownerEmail:'#'
            })
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>

    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body>
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="block-6">
          <div class="row">
        <div class="grid_7 suffix_1">
              <div class="map_wrapper">
            <h2 class="v1">How to Find Us</h2>
            <iframe id="map_canvas" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Brooklyn,+New+York,+NY,+United+States&amp;aq=0&amp;sll=37.0625,-95.677068&amp;sspn=61.282355,146.513672&amp;ie=UTF8&amp;hq=&amp;hnear=Brooklyn,+Kings,+New+York&amp;ll=40.649974,-73.950005&amp;spn=0.01628,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
            <div class="clearfix">
                  <div class="address">
                <dl>
                      <dt>8901 Marmora Road,<br>
                    Glasgow, D04 89GR. </dt>
                      <dd><span>Freephone:</span>+1 800 559 6580</dd>
                      <dd><span>Telephone:</span>+1 800 603 6035</dd>
                      <dd><span>FAX:</span>+1 800 889 9898</dd>
                      <dd><span class="e-mail">E-mail: <a href="#" class="demo">mail&#64;demolink.org</a></span></dd>
                    </dl>
              </div>
                  <div class="address">
                <dl>
                      <dt>9863 - 9867 Mill Road,<br>
                    Cambridge, MG09 99HT </dt>
                      <dd><span>Freephone:</span>+1 800 559 6580</dd>
                      <dd><span>Telephone:</span>+1 959 603 6035</dd>
                      <dd><span>FAX:</span>+1 800 889 9898</dd>
                      <dd><span class="e-mail">E-mail: <a href="#" class="demo">mail&#64;demolink.org</a></span></dd>
                    </dl>
              </div>
                </div>
          </div>
            </div>
        <article class="grid_4">
              <h2 class="v1">Contact Form</h2>
              <form id="contact-form" name="contact_form" method="post">
            <div class="success-message">Contact form submitted.<strong>We will be in touch soon.</strong></div>
            <label class="name">
                  <input type="text" placeholder="Name:" data-constraints="@Required @JustLetters" name="sender_name"/>
                  <span class="empty-message">*This field is required.</span> <span class="error-message">*This is not a valid name.</span> </label>
            <label class="email">
                  <input type="text" placeholder="E-mail:" data-constraints="@Required @Email" name="sender_email" id="sender_email"/>
                  <span class="empty-message">*This field is required.</span> <span class="error-message">*This is not a valid email.</span> </label>
            <label class="phone">
                  <input type="text" placeholder="Phone:" data-constraints="@Required @JustNumbers" name="sender_phone"/>
                  <span class="empty-message">*This field is required.</span> <span class="error-message">*This is not a valid phone.</span> </label>
            <label class="message">
                  <textarea placeholder="Message:" data-constraints='@Required @Length(min=20,max=999999)' name="sender_message"></textarea>
                  <span class="empty-message">*This field is required.</span> <span class="error-message">*The message is too short.</span> </label>
            <div class="form_buttons"> <a href="#" data-type="reset" class="link">clear</a> <a href="#" data-type="submit" class="link" 
            onclick="javascript:validate();">send</a> </div>
          </form>
            </article>
      </div>
        </div>
  </div>
    </section>
<script type="text/javascript">
	function validate () {
		if (document.getElementById('sender_email').value != '') { 
			document.getElementById("contact-form").submit();
			
		}else {
			return false;
		}
	}
</script>
<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>