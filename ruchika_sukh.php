<!DOCTYPE html>
<html lang="en">
    <head>
    <title>About us</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <?php $pageURL = 'http';
 	if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		$pageURL .= "://";
		$pageURL .= $_SERVER["SERVER_NAME"];
	?>
    <script type="text/javascript">
		function mail_f(){
			var content="";
			var err =0;
			var email =$("#email").val();
			var email_pattern = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
			if($("#first_name").val()==""){
				content +="Name is required."; 
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err = 0;
			}
			if($("#email").val()==""){
				content +="Email is required.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else if(!email_pattern.test(email)){
				content +="Enter correct email.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err=0;
			}
			if($("#message").val()==""){
				content +="Message is required.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err=0;
			}
			if(err==0){
			var first_name= $("#first_name").val();
			var last_name=$("#last_name").val();
			var message=$("#message").val();
			var mail_to="ruchikasukh@gmail.com";
			data="first_name=" + first_name +"&last_name=" +last_name +"&email=" +email+ "&message="+ message +"&mail_to=" +mail_to;
			$.ajax(
					{
						url : "<?=$pageURL?>/mail.php",
						type: "POST",
						data : data,
						success:function(data, textStatus, jqXHR)
						{	
							//alert(data);
							if(data=="TRUE"){
								$("#mail_result").show();
								content +="Mail sent successfully";
								$("#first_name").val('');
								$("#last_name").val('');
								$("#email").val('');
								$("#message").val('');
								
							}else if(data=="FALSE"){
								$("#mail_result").show();
								content +="Mail can not sent";
							}else{
								content +="Mail can not sent because of network problem";
							}
							$("#mail_result").show();
							$("#mail_result").text(content);	
							//data: return data from server
						},
						error: function(jqXHR, textStatus, errorThrown)
						{
							content +="Mail can not sent because of network problem";
							$("#mail_result").show();
							$("#mail_result").text(content);
							//if fails     
						}
					});
			}else{
				content +="enter value in field correctly";	
				$("#mail_result").show();
				$("#mail_result").text(content);
			}
			$("#mail_result").show();
			$("#mail_result").text(content);
		}
		function hide_error(){
		$("#mail_result").hide();
		}
    </script>
    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body>
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 manifestation ">
         <h2 class="v1">Ruchika Sukh Energy Essential Series</h2>
        <h3>LIFE PATH CLEARING @175$</h3>
        <ul class="li_point">
              <li>Session involves 2 – 3 hours of intensive work in reading your Akashic Records and
            clearing all blocks & restrictions. </li>
              <li>Includes 90 minute Skype session to tell you exactly what was holding you back and 
            what changes you need to make in your life. </li>
              <li> This is a super intensive session to give you a deep energy clearing especially if you are 
            feeling blocked in life for reasons you do not know! It will help you:</li>
              <li> Tap into your Soul's Divine Gifts & Life Purpose </li>
              <li> Clear all blocks & restrictions from present & past lives that hold you back</li>
              <li> Remove limiting beliefs, patterns and programs at the emotional, mental and spiritual 
            bodies </li>
              <li> Heal wounds & traumas to emotional, mental & spiritual bodies </li>
              <li> Ancestral healing for better family relations and a feeling of being supported</li>
              <li> Guidance from your Higher Self & Spirit Guides </li>
              <li> Reclaim your power to maximize your current choices </li>
              <li> Realign at Soul level to vibrate at your highest frequency</li>
              <li> This Clearing will set the stage for a quantum leap in your awareness</li>
              <li> This is not the end of the path, but a booster dose to push you to live your life as an 
            expression of your divinity!</li>
            </ul>
        <div class="full_width_wrap"> <a href="pay_now_ruchika.html" class="pay_btn">Pay Now</a></div>
        <h3>MONEY BLOCKS CLEARING @ 125$</h3>
        <ul class="li_point">
              <li>Session involves 1 – 2 hours of intensive work in reading your Akashic Records and
            clearing all blocks & restrictions. </li>
              <li> Includes 1 hour Skype session to tell you exactly what was holding you back and what 
            changes you need to make in your life. </li>
              <li> Activate your money intuition!</li>
              <li> Dissolve soul level blocks & restrictions to abundance in all areas of life </li>
              <li> Clear past patterns of loss and failure</li>
              <li> Realign yourself with abundance & success</li>
              <li>Release beliefs and programs of lack running in your mental & emotional bodies</li>
              <li> Know your soul gifts and learn how you can share them with the world, which will 
            naturally align you with abundance</li>
              <li> Clear the energy of your business to start attracting the opportunities that are aligned 
            with your highest good</li>
              <li> Know the exact changes you need to make to realign your life with abundance</li>
              <li> Confidently stand in service by boosting your self-worth </li>
              <li> Learn to actively own your value and let it reflect it in your work / business</li>
            </ul>
        <div class="full_width_wrap"> <a href="pay_now_ruchika125.html" class="pay_btn">Pay Now</a></div>
        <h3>LOVE AWAKENMENT @125$</h3>
        <ul class="li_point">
              <li> Session involves 1 - 2 hours of intensive work in reading your Akashic Records and
            clearing all blocks & restrictions. </li>
              <li> Includes 1 hour Skype session to tell you exactly what was holding you back and what 
            changes you need to make in your life. </li>
              <li> Release your hidden barriers to love!</li>
              <li> Heal your heart from past painful relationships </li>
              <li> Release toxic connections, cut cords of unhealthy attachment</li>
              <li> Remove walls in and around your heart that prevent you from loving fully</li>
              <li> Boost self-confidence, self-love & self-worth, learn to value Self</li>
              <li> Cleanse your heart to appreciate beauty within & outside </li>
              <li> Heal & Balance the Masculine - Feminine energies within</li>
              <li> Attune Your heart to the Divine Blueprint of Love </li>
              <li> Be more attractive to your divine complement/ twin flame </li>
              <li> Get ready to create 5th dimensional relationships</li>
            </ul>
        <div class="full_width_wrap"> <a href="pay_now_ruchika125.html" class="pay_btn">Pay Now</a></div>
        <h3>RELATIONSHIP READING & CLEANSE @125$</h3>
        <p class="pCustom">*For Couples, Parents and Children, Siblings, Business Partners or anyone who is in any kind of
              relationship with another person. </p>
        <p class="pCustom"> **You can get this reading as an individual or for both people.</p>
        <ul class="li_point">
              <li> Session involves 1 – 2 hours of intensive work in reading & clearing your Akashic
            Records. </li>
              <li> Includes 1 hour Skype session to tell you exactly what was holding you back and what 
            changes you need to make in your life. </li>
              <li> Understand how you present yourself and interact in your relationships to connect 
            more openly </li>
              <li> Resolve karmic entanglements in your relationship for greater harmony & love </li>
              <li> Cleanse the conscious and unconscious blocks that prevent mutual understanding & 
            connection</li>
              <li> Dissolve the obvious or subtle restrictions you may feel in your relationship</li>
              <li> Cut toxic ties to past connections </li>
              <li> Learn why you have attracted a particular relationship and what lessons it has to offer</li>
              <li> Understand your life lessons and learn how you can support each other’s journey</li>
            </ul>
        <div class="full_width_wrap"> <a href="pay_now_ruchika125.html" class="pay_btn">Pay Now</a></div>
        <div class="contact_frm">
              <div class="con_form">
            <h2>Contact For Further Enquiry</h2>
            <div id="mail_result"></div>
            <div class="full_formW">
                  <input type="text" name="first_name" id="first_name" onFocus="hide_error();" placeholder="First Name">
                </div>
            <div class="full_formW">
                  <input type="text" name="last_name" id="last_name" placeholder="Last Name">
                </div>
            <div class="full_formW">
                  <input type="text" name="email" id="email" onFocus="hide_error();" placeholder="Email" >
                </div>
            <div class="full_formW">
                  <textarea name="message" id="message" onFocus="hide_error();" placeholder="Message"></textarea>
                </div>
            <div class="full_formW">
                  <button onClick="mail_f();">Submit</button>
                </div>
          </div>
            </div>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>