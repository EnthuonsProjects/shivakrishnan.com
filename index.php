﻿<!DOCTYPE html>

<html lang="en">
    <head>
    <title>Spiritual Counseling | Bio Energy healing | Energy healing Technique | Healing Symbols | Spiritual Energy Healing</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/camera.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">

    <!-- <link rel="stylesheet" href="css/touchTouch.css"> -->

    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/camera.js"></script>
    <script src="js/jquery.quovolver.min.js"></script>
    <script>
  $(document).ready(function() {
    $('.quotes').quovolver({
      children    : 'li',
      transitionSpeed : 200,
	  autoPlaySpeed : 5000,
      autoPlay    : true,
      equalHeight   : false,
      navPosition   : 'above',
      navPrev     : false,
      navNext     : false,
      navNum      : false,
    });
    
  });
  </script>

    <!--[if (gt IE 9)|!(IE)]><!-->

    <script src="js/jquery.mobile.customized.min.js"></script>

    <!--<![endif]-->

    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>

    <!-- // <script src="js/touchTouch.jquery.js"></script> -->

    <script src="js/TMForm.js"></script>
    <script src="js/sForm.js"></script>
    <script>

        $(window).load(function() {

//            jQuery('#camera_wrap_1').camera({

//                height: '66.4%',

//                playPause: false,

//                time: 8000,

//                transPeriod: 1000,

//                fx: 'simpleFade',

//                loader: 'none',

//                minHeight:'150px',

//                navigation: false,

//                pagination: false,

//            });

            $('#newsletter1').sForm({

                ownerEmail:'#',

                sitename:'sitename.link'

            });

            $().UItoTop({ easingType: 'easeOutQuart' });

        });

    </script>

    <!--[if lt IE 8]>

       <div style=' clear: both; text-align:center; position: relative;'>

         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">

           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />

         </a>

      </div>

    <![endif]-->

    <!--[if lt IE 9]>

        <script src="js/html5shiv.js"></script>

        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">

    <![endif]-->

    </head>

    <body>

<!--==============================header=================================-->

<header class="page-1">
      <?php require('blog/wp-blog-header.php'); ?>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 clearfix">
        <div class="clearfix fleft sn_logo">
              <h1><a href="index.php"><img src="images/logo.png" alt="shiva krishnan" title="shiva krishnan" /></a></h1>
              <div class="tagLine">‘Manifestation is not about creating an item or a situation in your life. It’s entirely about becoming the vibration at which what you desire already exists.’
            <div class="right_go"> - Shiva Krishnan </div>
          </div>
            </div>
        <nav class="clearfix">
              <form id="search1" class="search" action="<?php bloginfo('url'); ?>/" method="GET">
            <div>
                  <input type="text" name="s">
                  <a onClick="document.getElementById('search1').submit()" class="button1"><i class="icon-search"></i></a> </div>
          </form>
              <ul class="sf-menu clearfix">
            <li class="current"><a href="index.php">Home</a></li>
            <li><a href="index-1.php">About us</a></li>
            <li><a href="blog/index.php">Blog</a></li>
          </ul>
            </nav>
      </div>
        </div>
    <div class="clearfix">
          <div class="grid_12"> 
          <figure class="banner_img"><img src='images/slide-1.jpg' alt='' /></figure>
          <div id="banner_text" class="slider-text">
          <div class="s-text-1">You can live a better life</div>
          <div class="s-text-2 cust_head">Or you can choose to remain as you are. </div>
          <a href="#area_trs" class="s-btn">Click Here To Begin The Transformation</a> 
          </div>
          </div>
        </div>
  </div>
    </header>

<!--=======content================================-->

<section class="content">
      <div class="container_12">
    <div class="container_12">
          <div class="row">
        <div class="grid_4 box-1 plr25"> <i class="icon-comments-alt"></i>
              <h2><a href="index-1.php">What Can We Do For You?</a></h2>
              <div>
              
              At its basis, all of creation is but energy alone. Creation comes into material manifestation by energy vibrating at different frequencies.

               </div>
              <a href="index-1.php#weare" class="link">more info</a> </div>
        <div class="grid_4 box-1 plr25"> <i class="icon-envelope-alt"></i>
              <h2>Free Downloads<br>
            &nbsp; </h2>
              <div>Join our mailing list to access powerful binaural beats audio downloads free.</div>
              <form id="newsletter1">
            <div class="success">Your subscription request<br>
                  has been sent!</div>
            <fieldset>
                  <label class="email">
                <input type="email" value="ENTER YOUR EMAIL">
                <span class="error">*This is not a valid email address.</span> </label>
                  <a href="#" data-type="submit"></a>
                </fieldset>
          </form>
             </div>
        <div class="grid_4 box-1 plr25"> <i class="icon-group"></i>
              <h2>Blog<br>
            Like to Read ?</h2>
              <ul class="blogLinks customLi">
            <ul>
                  <?php
	$args = array( 'numberposts' => '3' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
	}
?>
                </ul>
          </ul>
              <a href="http://shivakrishnan.com/blog/index.php" class="link">more info</a> </div>
      </div>
        </div>
  </div>
      <div class="container_12">
    <div class="row">
          <div class="bord-1">
        <div class="clearfix">
              <div class="grid_7 suffix_1">
            <div class="title-1">Are you ready <br>
                  to transform your life?</div>
            <div><br>
                 ‘There are no mistakes in life!’ <br><br>

                  The culmination of every breath, every thought, every action, every experience in life has brought you to this point, to this website, reading this piece of writing. There are no mistakes in life. No coincidences. Your birth in this world was not a mistake. <strong>It’s time to awaken to your true purpose.</strong>  <br>
                  <br>
                 Your soul has risen in vibration enough to bring to your awareness the possibility of a life that has been waiting for you to claim all this time. To claim, all you need to do is become the vibration where your desires become your reality. It truly is this simple.  <br>
                  <br>
                 Are you ready to transform your life?  <br>
                  <br>
                </div>
            <!-- <div class="link"><a href="#">Click Here to begin the transformation.</a></div>--> 
          </div>
              <div class="grid_4">
            <div class="block-1">
                  <div class="some_big_f">
                <div class="quotes">
                      <ul>
                    <li> <span class="quote_s">“</span> Your soul is oftentimes a battlefield, upon which your reason and your judgement wage war against your passion and your appetite.<br>
                          <br>
                          Would that I could be the peacemaker in your soul, that I might turn the discord and the rivalry of your elements into one-ness and melody <span class="quote_s">”</span>
                          <div class="right_go"> - Kahlil Gibran </div>
                        </li>
                    <li> <span class="quote_s">“</span> Oh my God, Oh my Universe, I just can’t believe what you have done for me. Shiva, what you have said makes complete sense to me. Thank you so much! My husband for the first time in eleven years is finally making an effort for his wife and his daughter. So much Love <span class="quote_s">”</span>
                          <div class="right_go"> - SJ </div>
                        </li>
                    <li> <span class="quote_s">“</span> To Whom It May Concern, I’m usually a HUGE skeptic to these sort of things. I mean, energy work seems all hocus Pocus to me. Anyway, my husband thinks very highly of Shiva. Mind you, Shiva is a really nice guy, not the gypsy type at all. So, I had a meridian flush session with Shiva. I just cannot explain how it has changed my life. I could feel something happening when he was doing the work, tingles on my skin, my hair felt like someone was touching it. Since the session life has been amazing. I feel so much lighter. My family and friends behave differently around me. My skin has cleared up. I could go on and on. If you want to change your life, have a session. As shiva says, the time to take action is now. <span class="quote_s">”</span>
                          <div class="right_go"> - Maria Peck </div>
                        </li>
                    <li> <span class="quote_s">“</span> I personally testify that Shiva’s ‘neurological ascension programming’ absolutely works. He asked me the sort of life I want to live and the things I want to manifest. He created the program for me and downloaded it into my being. What can I say? I can feel the re-wiring happening within me. Emotional blocks are lifting. I’ve been so lazy most of my life and here I am within days getting up and hitting the gym at 4am. My wife can’t believe it. I haven’t yet told her what we’ve done. People are behaving completely differently around me. NAP absolutely works. <span class="quote_s">”</span>
                          <div class="right_go"> - Travis Chan </div>
                        </li>
                    <li> <span class="quote_s">“</span> Shiva, I can’t thank you enough! The transformation that has occurred within me after my session with you, has been tremendous. It’s an amazing feeling to experience life full of clarity, energy and vitality that I had not possessed previously. No longer do I see this life being a constant struggle, but rather a journey and a path to fulfilment.  It has been an amazing journey to finally get to know the real me. Your gift of being a healer is far greater, for you didn’t just heal me, you did one better - you taught me how to heal myself. I can honestly say that I am truly blessed to have met you and to have been showered with your guidance, support and inspiration. It is not every day you meet someone or speak to someone that understands your needs completely, especially at a spiritual level <span class="quote_s">”</span>
                          <div class="right_go"> - SM </div>
                        </li>
                    <li> <span class="quote_s">“</span> I have known Shiva for years now. He is my go to guy for all things spiritual in nature. I consider him to be my link to my soul. Over the years he has done a number of clearings and healing’s for me. They work amazingly well. I am a high frequency being and have so much trouble assimilating into this world and so need lots of grounding work. No one is able to do this for me except Shiva. He has a true gift. Oh and ask him to create a shield for you, it is the bomb! Love you Shiv <span class="quote_s">”</span>
                          <div class="right_go"> - Brenda Hays </div>
                        </li>
                    <li> <span class="quote_s">“</span> Shiva did a Meridian Flush for myself and my son recently. I feel so much better. My energy is lighter and I generally feel so much happier. My son had been suffering from hay fever for many years and it seems to have cleared up after Shivas session. We think it might have been due to being intolerant to gluten. Since the clearing he has been eating breads without any reaction. Fingers crossed! <span class="quote_s">”</span>
                          <div class="right_go"> - Anjali and KN </div>
                        </li>
                    <li> <span class="quote_s">“</span> I’ve had a NAP session with Shiva. My being is completely transforming. I noticed my behaviour towards certain annoyances has changed. My coffee addiction is gone. I’ve slowed down on smoking and am actually feeling like I don’t want to smoke anymore. Shiva programmed that the body re-energise automatically. I love this feeling so much. I don’t feel tired anymore. It has been the greatest gift. I can actually feel my vibration lifting to living my dreams. This would have to be the greatest find. <span class="quote_s">”</span>
                          <div class="right_go"> - Amy Banes</div>
                        </li>
                    <li> <span class="quote_s">“</span> Shiva rocks! That is an understatement. He has created shields for me in the past and they work like a charm. I can literally feel the energy rising and enveloping me. Nothing gets past Shiva’s shields. <span class="quote_s">”</span>
                          <div class="right_go"> - Brad</div>
                        </li>
                    <li> <span class="quote_s">“</span> I have had the pleasure of knowing Shiva for over ten years now. I can say for someone his age he is possibly the wisest person I have ever met. Behind his jokes and the most alluring laughter, there is a wisdom that is absolutely divine. He has an innate ability to say the right things at the right time. I love the way he says ‘I am simply a mirror to your soul’. He may only be 34, yet when he speaks he sounds like a 100 year old. <span class="quote_s">”</span>
                          <div class="right_go"> - Devi K</div>
                        </li>
                    <li> <span class="quote_s">“</span> Ever have a person, a complete stranger come out of nowhere and touch your soul like no one ever has before? The universe completely shocked me by crossing my path with an Earth Angel like Shiva. His presence alone has completely transformed my very being. I cannot say enough good things about him. If the universe destines you to cross paths with someone like Shiva, then you are an extremely lucky soul. Be blessed. <span class="quote_s">”</span>
                          <div class="right_go"> - Ashoka Matre</div>
                        </li>
                    <li> <span class="quote_s">“</span>I have a business which had been struggling for sometime. I initially approached Shiva for a personal session. After the session I felt absolutely amazing, inspired, motivated, and full of passion. A few days ago I decided to have Shiva do a session on my business. I already feel a difference in energy at our premises. My staff seem more vibrant. The biggest change has been in the way my clients have been. Discussions have begun for new possible projects. Doors are opening up. I believe it is entirely the work that Shiva did for me. <span class="quote_s">”</span>
                          <div class="right_go"> - Savicharan Sachdev</div>
                        </li>
                    <li> <span class="quote_s">“</span>Absolutely changed my life! Changed the way I think! Changed the way I deal with everyone around me! Thank you, Thank you, my dear Shiva for liberating my soul! <span class="quote_s">”</span>
                          <div class="right_go"> - Bianca Jeisman</div>
                        </li>
                    <li> <span class="quote_s">“</span>I did the Ultimate Manifestation Session with Shiva Krishnan. I can see and feel the changes happening in and around me daily. I call them my little miracles. I don’t care how it all works, I just know this is the real deal. Shiva, you are my God lol<span class="quote_s">”</span>
                          <div class="right_go"> - Sarah</div>
                        </li>
                    <li> <span class="quote_s">“</span>The Ultimate Manifestation Session (UMS) has opened doors outside/inside of me. It has brought in so much of shift, in my awareness.I feel a tremendous increase in my ability to love myself unconditionally, completely. Consequently to love others, too! I am so much more joyous and positive!<br>
                          <br>
                          I am in my element! <br>
                          <br>
                          I am so inspired - that I have started making changes in all aspects of my life-relationship, financial, health, parenting et al! <br>
                          <br>
                          The UMS led by Shiva K helped me to 'realise' that I was clenching tightly to-quite many negative notions about myself- the central one being-that I was not 'good enough' to deserve nice things in life! It is such a beautiful feeling to first 'become aware of such negative labels' and then FINALLY to "let go of them, all!!!"<br>
                          <br>
                          My friends and relatives who have had the UM session have all singularly reported  big, big, big dollops of positivity and clarity in mind, higher energy levels... and overall 'healing'- of mind & soul!<br>
                          <br>
                          Thank a googol, Shiva Krishnan. You are a veracious Jesus & Buddha & Sri Sri Ravi Shankar-all rolled into one. A beautiful gift from existence to all of Humanity! You changed my world (inner/outer)-forever! Thank ‘you'!!!<span class="quote_s">”</span>
                          <div class="right_go"> - P. J. Bala Ramya Rohini</div>
                        </li>
                  </ul>
                    </div>
              </div>
                </div>
          </div>
            </div>
      </div>
        </div>
    <div class="block-2 san_bl">
          <h2 class="do_one">Clearing and Healing Sessions </h2>
          <div class="row">
        <div class="grid_6 box-2 maxheight" >
              <div class="clearfix"> <img src="images/page-1_img-1.jpg" alt="" class="img-ind w1"/>
            <div>
                  <h2 id="area_trs">Ultimate Clearing Session</h2>
                  <div>This removes all major energetic blocks affecting all areas of life. A MUST for everyone!</div>
                  <a href="ultimate_clearing.php" class="link">more info</a> </div>
          </div>
            </div>
        <!--<div class="grid_6 box-2 maxheight">
              <div class="clearfix"> <img src="images/page-1_img-2.jpg" alt="" class="img-ind w1"/>
            <div>
                  <h2>DNA Activation</h2>
                  <div>Awaken your divine abilities hidden in your DNA. Activate and heal all Twelve strands of DNA.</div>
                  <a href="#" class="link">more info</a> </div>
          </div>
            </div>-->
        <div class="grid_6 box-2 maxheight">
              <div class="clearfix"> <img src="images/page-1_img-3.jpg" alt="" class="img-ind w1"/>
            <div>
                  <h2>Ultimate Manifestation Session [UMS] Intense Transformation!</h2>
                  <div>INTENSELY POWERFUL! Serious about changing your life? Manifest the life you desire on Autopilot! </div>
                  <a href="ultimate_manifestation.php" class="link">more info</a> </div>
          </div>
            </div>
        <div class="grid_6 box-2 maxheight">
              <div class="clearfix"> <img src="images/page-1_img-4.jpg" alt="" class="img-ind w1"/>
            <div>
                  <h2>Ultimate Energy Session</h2>
                  <div> A powerful healing session. This clears and heals all 72,000 meridians and every chakra they pass. </div>
                  <a href="ultimate_energy.php" class="link">more info</a> </div>
          </div>
            </div>
        <div class="grid_6 box-2 maxheight mah_mer">
              <div class="clearfix"> <img src="images/page-1_img-5.jpg" alt="" class="img-ind w1"/>
            <div>
                  <h2>Ruchika Sukh Energy Essential Series</h2>
                  <div >
                <ul>
                      <li>Life Path Clearing</li>
                      <li>Love Awakenment</li>
                      <li>Abundance Block Clearing</li>
                      <li>Relationship Reading and Cleansing</li>
                      <li>House / Property Clearing</li>
                    </ul>
                <a href="ruchika_sukh.php"  class="link">more info</a> </div>
                </div>
          </div>
            </div>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>