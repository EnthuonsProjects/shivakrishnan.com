<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Privacy Policy</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="css/contacts.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>

    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body>
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_12">
        <h2 class="v1">Privacy Policy</h2>
        <div class="text-2">Vivamus eget tristique arcu, a hendrerit est. Aliquam imperdiet, est sit amet tempus convallis, nisl augue placerat ligula.</div>
        <p>Pellentesque aliquet nulla id neque pharetra, eu hendrerit diam placerat. Suspendisse suscipit erat sed tellus dignissim lobortis. Mauris at ante nec enim gravida consectetur ut et risus. Suspendisse sem dolor, varius a tortor ac, venenatis sodales neque. Nam aliquam sagittis tincidunt. Integer eu turpis ante. Donec porta in turpis sed placerat. Praesent faucibus, odio imperdiet dignissim eleifend, risus ipsum ornare lectus, ut condimentum tortor magna vel ipsum. Fusce nec nisi id arcu pharetra euismod. Vivamus posuere nisl sed mi mollis, id interdum libero consequat. Cras ac nisl sem. Fusce vitae malesuada lorem. Maecenas a tellus id odio accumsan congue quis a massa. Sed vestibulum dapibus dui, in mollis mauris tempor id. Praesent ut nisi ac ipsum vehicula porta sit amet in enim. Mauris semper viverra varius. Etiam viverra urna vitae ultricies dapibus. In hac habitasse platea dictumst. Pellentesque ultricies vulputate lacinia. Phasellus laoreet, purus sit amet mollis tempus, lacus turpis sagittis nisi, in pretium purus diam eget lectus. Morbi blandit laoreet fringilla. Pellentesque in massa odio. Sed eget condimentum tortor, eu dictum nisi. Integer elementum, ligula eu molestie tincidunt, massa nibh scelerisque ipsum, non sollicitudin leo purus non sapien.Integer eu augue non neque sed condimentum volutpat, libero erat iaculis elit, sed accumsan felis nunc ac nunc. Suspendisse non neque at diam mattis suscipit. Donec laoreet suscipit sapien, vitae rutrum quam pellentesque in. Morbi facilisis, urna ac malesuada fringilla, leo ante mattis orci. Pellentesque aliquet.</p>
        <div class="text-2">Suspendisse suscipit erat sed tellus dignissim lobortis. Mauris at ante nec enim gravida consectetur ut et risus.</div>
        <p>Suspendisse sem dolor, varius a tortor ac, venenatis sodales neque. Nam aliquam sagittis tincidunt. Integer eu turpis ante. Donec porta in turpis sed placerat. Praesent faucibus, odio imperdiet dignissim eleifend, risus ipsum ornare lectus, ut condimentum tortor magna vel ipsum. Fusce nec nisi id arcu pharetra euismod. Vivamus posuere nisl sed mi mollis, id interdum libero consequat. Cras ac nisl sem. Fusce vitae malesuada lorem. Maecenas a tellus id odio aliquam gravida non ac sapien. Suspendisse potenti. Sed posuere augue arcu, et pellentesque tellus placerat a.In vel viverra lectus. Nullam et malesuada felis. Curabitur leo eros, semper eu nibh eget, porta posuere orci. Aliquam sagittis sapien ac lectus hendrerit, ornare porta nulla lacinia. Sed vitae orci vitae nisi accumsan congue quis a massa. Sed vestibulum dapibus dui, in mollis mauris tempor id. Praesent ut nisi ac ipsum vehicula porta sit amet in enim. Mauris semper viverra varius. Etiam viverra urna vitae ultricies dapibus. In hac habitasse platea dictumst. Pellentesque ultricies vulputate lacinia. Phasellus laoreet, purus sit amet mollis tempus, lacus turpis sagittis nisi, in pretium purus diam eget lectus. Morbi blandit laoreet fringilla. Pellentesque in massa odio. Sed eget condimentum tortor, eu dictum nisi. Integer elementum, ligula eu molestie tincidunt, massa nibh scelerisque ipsum, non sollicitudin leo purus non sapien.Integer eu augue non nisl cursus tempor eget vel ligula. Donec metus erat, faucibus vel lobortis mattis, lobortis ut nunc. Duis porta convallis purus eget condimentum. Nulla aliquet, neque sed condimentum.</p>
        <div class="text-2">Suspendisse non neque at diam mattis suscipit. Donec laoreet suscipit sapien, vitae rutrum quam pellentesque in. </div>
        <p>Morbi facilisis, urna ac malesuada fringilla, leo ante mattis orci, in dapibus diam ipsum ac velit. Pellentesque aliquet nulla id neque pharetra, eu hendrerit diam placerat. Suspendisse suscipit erat sed tellus dignissim lobortis. Mauris at ante nec enim gravida consectetur ut et risus. Suspendisse sem dolor, varius a tortor ac, venenatis sodales neque. Nam aliquam sagittis tincidunt. Integer eu turpis ante. Donec porta in turpis sed placerat. Praesent faucibus, odio imperdiet dignissim eleifend, risus ipsum ornare lectus, ut condimentum tortor magna vel ipsum. Fusce nec nisi id arcu pharetra euismod. Vivamus posuere nisl sed mi mollis, id interdum libero consequat. Cras ac nisl sem. Fusce vitae malesuada lorem. Maecenas a tellus id odio aliquam gravida non ac sapien. Suspendisse potenti. Sed posuere augue arcu, et pellentesque tellus placerat a.In vel viverra lectus. Nullam et malesuada felis. Curabitur leo eros, semper eu nibh eget, porta posuere orci. Aliquam sagittis sapien ac lectus hendrerit, ornare porta nulla lacinia. Sed vitae orci vitae nisi accumsan congue quis a massa. Sed vestibulum dapibus dui, in mollis mauris tempor id. Praesent ut nisi ac ipsum vehicula porta sit amet in enim. Mauris semper viverra varius. Etiam viverra urna vitae ultricies dapibus. In hac habitasse platea dictumst. Pellentesque ultricies vulputate lacinia. Phasellus laoreet, purus sit amet mollis tempus, lacus turpis sagittis nisi, in pretium purus diam eget lectus. Morbi blandit laoreet fringilla. Pellentesque in massa odio. Sed eget condimentum tortor, eu dictum nisi. Integer elementum, ligula eu molestie.</p>
        <div class="text-2">Duis porta convallis purus eget condimentum. Nulla aliquet, neque sed condimentum volutpat, libero erat iaculis elit, sed.</div>
        <p>Suspendisse non neque at diam mattis suscipit. Donec laoreet suscipit sapien, vitae rutrum quam pellentesque in. Morbi facilisis, urna ac malesuada fringilla, leo ante mattis orci, in dapibus diam ipsum ac velit. Pellentesque aliquet nulla id neque pharetra, eu hendrerit diam placerat. Suspendisse suscipit erat sed tellus dignissim lobortis. Mauris at ante nec enim gravida consectetur ut et risus. Suspendisse sem dolor, varius a tortor ac, venenatis sodales neque. Nam aliquam sagittis tincidunt. Integer eu turpis ante. Donec porta in turpis sed placerat. Praesent faucibus, odio imperdiet dignissim eleifend, risus ipsum ornare lectus, ut condimentum tortor magna vel ipsum. Fusce nec nisi id arcu pharetra euismod. Vivamus posuere nisl sed mi mollis, id interdum libero consequat. Cras ac nisl hendrerit, ornare porta nulla lacinia. Sed vitae orci vitae nisi accumsan congue quis a massa. Sed vestibulum dapibus dui, in mollis mauris tempor id. Praesent ut Pellentesque ultricies vulputate lacinia. Phasellus laoreet, purus sit amet mollis tempus, lacus turpis sagittis nisi, in pretium purus diam eget lectus. Morbi blandit laoreet fringilla. Pellentesque in massa odio. Sed eget condimentum tortor, eu dictum nisi. Integer elementum, ligula eu molestie tincidunt, massa nibh scelerisque ipsum, non sollicitudin leo purus non sapien.Integer eu augue non nisl cursus tempor eget vel ligula. Donec metus erat, faucibus vel lobortis mattis, lobortis ut nunc. Duis porta convallis purus eget condimentum. Nulla aliquet, neque sed condimentum volutpat, libero erat iaculis elit, sed accumsan felis nunc ac nunc. </p>
        <div><a href="#" class="underline">privacy@demolink.org</a></div>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
<!--Coded by Katerina-->
</body>
</html>