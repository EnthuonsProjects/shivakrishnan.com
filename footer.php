<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_3 fright f-top"> <span class="f-text-1">Follow Us</span>
        <ul class="list-services clearfix">
          <li><a href="#"><i class="icon-google-plus-sign"></i></a></li>
          <li><a href="#"><i class="icon-twitter-sign"></i></a></li>
          <li><a href="#"><i class="icon-facebook-sign"></i></a></li>
          <li><a href="#"><i class="icon-rss-sign"></i></a></li>
        </ul>
      </div>
      <div class="grid_3 fright f-top box-3">
        <div><i class="icon-home"></i> <a href="mailto:shiva@shivakrishnan.com">shiva@shivakrishnan.com</a> <br>
          <a href="mailto:ruchika@shivakrishnan.com">ruchika@shivakrishnan.com</a></div>
        <div><i class="icon-phone"></i> Phone: +61 434 288 376</div>
      </div>
      <div class="grid_6 fright f-top">
        <ul class="list-1">
          <li><a href="index.php" >Home</a></li>
          <li><a href="index-1.php">About us</a></li>
          <li><a href="blog/index.php" >Blog</a></li>
        </ul>
        <a href="index.php" class="logo"><img title="shiva krishnan" alt="shiva krishnan" src="images/logo-footer.png"></a>
        <div class="copyright"> <a rel="nofollow" href="/">www.shivakrishnan.com </a></div>
      </div>
    </div>
  </div>
</footer>
