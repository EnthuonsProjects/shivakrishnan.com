<!DOCTYPE html>
<html lang="en">
    <head>
    <title>About us</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
  <script type="text/javascript">
		function mail_f(){ //alert("come");
		var first_name= $("#first_name").val();
		var last_name=$("#last_name").val();
		var email =$("#email").val();
		var message=$("#message").val();
		var mail_to="shiva.krishnan@optusnet.com.au";
		content="";
		data="first_name=" + first_name +"&last_name=" +last_name +"&email=" +email+ "&message="+ message +"&mail_to=" +mail_to;
		$.ajax(
				{
					url : "<?=$pageURL?>/dev/shiva_krishna2/mail.php",
					type: "POST",
					data : data,
					success:function(data, textStatus, jqXHR)
					{	
						//alert(data);
						if(data=="TRUE"){
							content +="Mail sent successfully";
						}else if(data=="FALSE"){
							content +="Mail can not sent";
						}else{
							content +="Mail can not sent because of network problem";
						}
						$("#mail_result").text(content);	
						//data: return data from server
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
						content +="Mail can not sent because of network problem";
						$("#mail_result").text(content);
						//if fails     
					}
				});
				
			 
		}
    </script>
    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body onLoad="document.form.submit();">
<!--==============================header=================================-->
<header>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 clearfix">
        <div class="clearfix fleft sn_logo">
              <h1><a href="index.php"><img src="images/logo.png" alt="shiva krishnan" title="shiva krishnan" /></a></h1>
              <div class="tagLine">‘Manifestation is not about creating an item or a situation in your life. It’s entirely about becoming the vibration at which what you desire already exists.’
            <div class="right_go"> - Shiva Krishnan </div>
          </div>
            </div>
        <nav class="clearfix">
              <form id="search1" class="search" action="search.php" method="GET">
            <div>
                  <input type="text" name="s">
                  <a onClick="document.getElementById('search1').submit()" class="button1"><i class="icon-search"></i></a> </div>
          </form>
              <ul class="sf-menu clearfix">
            <li><a href="index.php">Home</a></li>
            <li class="current"><a href="index-1.html">About us</a></li>
            <li><a href="blog/index.php">Blog</a></li>
          </ul>
            </nav>
      </div>
        </div>
  </div>
    </header>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 manifestation ">
        <h2 class="v1"> Ultimate Clearing Session [UCS] Intense</h2>
        <div class="wrap_text">
              <ul class="one_li">
            <li>Facilitator </li>
            <li>Shiva Krishnan</li>
            <div class="clear"></div>
            <li>Mode</li>
            <li>Distance</li>
            <div class="clear"></div>
            <h3>Self Investment USD</h3>
            <div class="clear"></div>
            <li>Individual</li>
            <li>$125</li>
            <div class="clear"></div>
            <li>Family </li>
            <li>$290 [Family of four]</li>
            <div class="clear"></div>
            <li>Business: </li>
            <li>$399</li>
            <div class="clear"></div>
            <li>Duration: </li>
            <li>90 Minutes</li>
          </ul>
              <p>The highest level of energy attunement available! </p>
              <p>Everything at its basis is energy, including stress, worries, blocks to wealth, happiness, physical ailments, career issues, relationship problems, past experiences, etc. This sits within various aspects of our being, often causing further disruptions in our present lives. To remove these issues from your life you must flush out the energies that hinder. </p>
              <p>This is an intensive energy attunement that clears your energies and begins the process of healing. The energy flow is thousands of times more powerful and specific in intent than Reiki. </p>
              <p>Some of the benefits include:</p>
              <p>Say good bye to bodily fatigue! Your body will automatically balance energy levels to ensure you are automatically sustained all of the time.
            Revitalise the life force within your body as we clear all of the 72,000 meridians.</p>
              <ul class="teo_ul">
            <li>Heal health issues.</li>
            <li> Be freed from all past emotional issues that have been a burden within your present.</li>
            <li> Be able to sleep better at night. Amazing for insomniacs! </li>
            <li> Heal muscle and joint pain.</li>
            <li> Heal past relationships, negative experiences, emotional burdens. This is quite liberating as you begin to feel much lighter due to the burden lifting.</li>
            <li> Heal emotional issues in present relationships.</li>
            <li> Balance hormone levels.</li>
            <li> Achieve mental clarity.</li>
            <li> Complete family harmony.</li>
            <li>Re-open the doors to communication amongst family members.</li>
            <li> Release negative belief systems, negative patterns, and negative connections.</li>
            <li> Re-vitalise business, opening doors to new opportunities.</li>
          </ul>
              <p>This is a partial list. The total benefits are endless. Each session is tailored entirely to the individual, family, or business. </p>
              <p>Please contact Shiva Krishnan directly to confidentially discuss your situation. </p>
              <div class="full_width_wrap"><a href="pay_Individual125.html" class="pay_btn">Pay Individual</a> <a href="pay_family_session390.html" class="pay_btn">Pay Family Session</a></div>
            </div>
        <div class="contact_frm">
              <div class="con_form">
            <h2>Contact For Further Enquiry</h2>
            <div id="mail_result"></div>
            <div class="full_formW">
                  <input type="text" name="first_name" id="first_name" placeholder="First Name" required autofocus>
                </div>
            <div class="full_formW">
                  <input type="text" name="last_name" id="last_name" placeholder="Last Name">
                </div>
            <div class="full_formW">
                  <input type="email" name="email" id="email" placeholder="Email" required>
                </div>
            <div class="full_formW">
                  <textarea name="message" id="message" placeholder="Message"></textarea>
                </div>
            <div class="full_formW">
                  <button onClick="mail_f();">Submit</button>
                </div>
          </div>
            </div>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<footer>
      <div class="container_12">
    <div class="row">
          <div class="grid_3 fright f-top"> <span class="f-text-1">Follow Us</span>
        <ul class="list-services clearfix">
              <li><a href="#"><i class="icon-google-plus-sign"></i></a></li>
              <li><a href="#"><i class="icon-twitter-sign"></i></a></li>
              <li><a href="#"><i class="icon-facebook-sign"></i></a></li>
              <li><a href="#"><i class="icon-rss-sign"></i></a></li>
            </ul>
      </div>
          <div class="grid_3 fright f-top box-3">
        <div><i class="icon-home"></i> <a href="mailto:sk@shivakrishnan.com">sk@shivakrishnan.com</a> <br>
              <a href="mailto:rs@shivakrishnan.com">rs@shivakrishnan.com</a></div>
        <div><i class="icon-phone"></i> Phone: +61 434 288 376</div>
      </div>
          <div class="grid_6 fright f-top">
        <ul class="list-1">
              <li><a href="index.php" class="current">Home</a></li>
              <li><a href="index-1.html">about us</a></li>
              <li><a href="blog/index.php">Blog</a></li>
            </ul>
        <a href="index.php" class="logo"><img title="shiva krishnan" alt="shiva krishnan" src="images/logo-footer.png"></a>
        <div class="copyright"> <a rel="nofollow" href="/">www.shivakrishnan.com </a></div>
      </div>
        </div>
  </div>
    </footer>
</body>
</html>
