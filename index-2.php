<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Life coaching</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>

    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body>
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1">
    <div class="container_12">
          <div class="block-4">
        <div class="row">
              <div class="grid_3"> <i class="icon-star"></i>
            <h2>Coaching Strategy Sessions</h2>
            <div>Praesent vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius.</div>
            <a href="#" class="link">more info</a> </div>
              <div class="grid_3"> <i class="icon-signal"></i>
            <h2>Success Coaching Programs</h2>
            <div>Vestibulum molestie lacus. Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. </div>
            <a href="#" class="link">more info</a> </div>
              <div class="grid_3"> <i class="icon-time"></i>
            <h2>Time <br>
                  Management</h2>
            <div>Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus.</div>
            <a href="#" class="link">more info</a> </div>
              <div class="grid_3"> <i class="icon-suitcase"></i>
            <h2>Business <br>
                  Coaching</h2>
            <div>Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus.</div>
            <a href="#" class="link">more info</a> </div>
            </div>
      </div>
        </div>
  </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_8">
        <h2>The Concepts of Coaching</h2>
        <div class="block-5">
              <div class="clearfix"> <img src="images/page-3_img-1.jpg" alt="" class="img-ind w1"/>
            <div>
                  <div class="text-2">Praesent vestibulum molestie lacus. </div>
                  <div>Aenean semper leo massa, eget placerat metus interdum nec. nostra, per inceptos himenaeos. Integer dui dui, suscipit ornare congue, lorem diam molestie diam, mattis tempor orci augue vitae velit. Nunc vel urna sem. </div>
                  <a href="#" class="link">more info</a> </div>
          </div>
              <div class="clearfix"> <img src="images/page-3_img-2.jpg" alt="" class="img-ind w1"/>
            <div>
                  <div class="text-2">Aenean semper leo massa, eget placerat metus.</div>
                  <div>Interdum nec. nostra, per inceptos himenaeos. Integer dui dui, In et purus eu lacus fringilla fermentum et sit amet diam. Sed convallis, tellus sit amet euismod bibendum, arcu arcu eleifend nisl, ut tincidunt turpis neque ac lacus. </div>
                  <a href="#" class="link">more info</a> </div>
          </div>
            </div>
      </div>
          <div class="grid_4">
        <h2>Achieve the Results You Desire</h2>
        <ul class="list">
              <li><a href="#">Nunc quis sagittis orci ac pretium</a></li>
              <li><a href="#">Phasellus sagittis nisi nulla</a></li>
              <li><a href="#">Nunc auctor justo non varius iaculis</a></li>
              <li><a href="#">Aliquam dapibus pharetra lacinia</a></li>
              <li><a href="#">Maecenas mattis arcu eu tincidunt</a></li>
              <li><a href="#">Praesent ac bibendum lorem</a></li>
              <li><a href="#">Nunc bibendum turpis ut sodales</a></li>
              <li><a href="#">Accumsan arcu erat accumsan</a></li>
              <li><a href="#">Mauris nec vestibulum lorem magna</a></li>
              <li><a href="#">Nulla fermentum, purus eu</a></li>
              <li><a href="#">Vverra cursus, eros ipsum hendrerit</a></li>
            </ul>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>