<!DOCTYPE html>
<html lang="en">
    <head>
    <title>About us</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>

    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body>
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 manifestation">
        <h2 class="v1">What Can We Do For You?</h2>
        <p> At its basis, all of creation is but energy alone. Creation comes into material manifestation by energy vibrating at different frequencies.<br>
              You in your entirety, inclusive of your body, all thoughts, choices, feelings, emotions, careers, opportunities, relationships, financial standing, physical ailments, etc, are all a mishmash of many energetic frequencies. <br>
              <br>
              To effect immediate change in any of these area’s of your being, change must be made at the root level, the energy itself. <br>
              <br>
              This is where we come in! We use the most advanced spiritual techniques, some created exclusively by us, to tune into and manipulate the energies. We’re able to remove energetic blocks that hinder everything from material success to physical health. Additionally, using our signature technique - Neurological Heart Brain Reprogramming - we’re able to reprogram the energies to vibrate at different frequencies resulting in instant manifestations. 
              
              We will change your life by changing your vibration. <br>
            </p>
        <h2 class="v1" id="weare"> Facilitator: Shiva Krishnan</h2>
        <p> Shiva Krishnan is an Advanced Energy Practitioner and Tantric Master with over twenty years of experience. He is a pioneer in the field of Alchemy and in the development of specific energy manipulation techniques. He is the brainchild behind the powerful ‘Ultimate Manifestation’ technique. <br>
              <br>
              Shiva is a highly intuitive being, naturally clairaudient, clairvoyant, and clairsentient. Beyond his spiritual pursuits, he is an entrepreneur, an Executive Advice Specialist, an author, a Feng Shui / Vastu Specialist, a tragic poet, philosopher, a self confessed prankster, and most importantly an empathic human being. <br>
              <br>
              "My purpose is to empower as many awakened human beings as possible. I do this by tapping into the core of your being and removing the very blocks that are hindering you from fully embodying your soul. Come begin a conversation with me, you may just discover the ‘answer’ to living the life of your dreams!” Shiva Krishnan</p>
        <h2 class="v1">Co-Facilitator: Ruchika Sukh</h2>
        <p> Ruchika Sukh is a  naturally gifted energy clearing specialist, Clinical Hypnotherapist, a soul level healer, empath & intuitive. <br>
              <br>
              She helps resolve your issues by revealing the ROOT CAUSE of any relationship, financial, work or health problem. She helps you be a trailblazer, a leader living life on your own terms, fulfilling your wildest dreams!<br>
              <br>
              You receive deep transformational insights about your divine gifts, higher purpose & any unconscious blocks preventing you from realising your potential!  She accesses your multi-dimensional Self via the Akashic Records  & not only retrieves key information but also clears and helps heals the unconscious.<br>
              <br>
              Ruchika has studied and practices various modalities including Soul Realignment, Spiritual Hypnotherapy, Yoga, Family & Systemic Constellations & Bio-Energy Correction. </p>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>