<!DOCTYPE html>
<html lang="en">
    <head>
    <title>About us</title>
    <meta charset="utf-8">
    <meta name = "format-detection" content = "telephone=no" />
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/superfish.css">
    <link rel="stylesheet" href="font/font-awesome.css" type="text/css" media="screen">
    <script src="js/jquery.js"></script>
    <script src="js/jquery-migrate-1.1.1.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/script.js"></script>
    <script src="js/superfish.js"></script>
    <script src="js/jquery.mobilemenu.js"></script>
    <script src="js/jquery.equalheights.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/sForm.js"></script>
    <script>
        $(window).load(function() {
            $('#newsletter1').sForm({
                ownerEmail:'#',
                sitename:'sitename.link'
            });
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
  <script type="text/javascript">
		function mail_f(){
			var content="";
			var err =0;
			var email =$("#email").val();
			var email_pattern = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
			if($("#first_name").val()==""){
				content +="Name is required."; 
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err = 0;
			}
			if($("#email").val()==""){
				content +="Email is required.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else if(!email_pattern.test(email)){
				content +="Enter correct email.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err=0;
			}
			if($("#message").val()==""){
				content +="Message is required.";
				$("#mail_result").show();
				$("#mail_result").text(content);
				err=1;
				return false;
			}else{
				err=0;
			}
			if(err==0){
			var first_name= $("#first_name").val();
			var last_name=$("#last_name").val();
			var message=$("#message").val();
			var mail_to="shiva.krishnan@optusnet.com.au";
			data="first_name=" + first_name +"&last_name=" +last_name +"&email=" +email+ "&message="+ message +"&mail_to=" +mail_to;
			$.ajax(
					{
						url : "<?=$pageURL?>/mail.php",
						type: "POST",
						data : data,
						success:function(data, textStatus, jqXHR)
						{	
							//alert(data);
							if(data=="TRUE"){
								$("#mail_result").show();
								content +="Mail sent successfully";
								$("#first_name").val('');
								$("#last_name").val('');
								$("#email").val('');
								$("#message").val('');
								
							}else if(data=="FALSE"){
								$("#mail_result").show();
								content +="Mail can not sent";
							}else{
								content +="Mail can not sent because of network problem";
							}
							$("#mail_result").show();
							$("#mail_result").text(content);	
							//data: return data from server
						},
						error: function(jqXHR, textStatus, errorThrown)
						{
							content +="Mail can not sent because of network problem";
							$("#mail_result").show();
							$("#mail_result").text(content);
							//if fails     
						}
					});
			}else{
				content +="enter value in field correctly";	
				$("#mail_result").show();
				$("#mail_result").text(content);
			}
			$("#mail_result").show();
			$("#mail_result").text(content);
		}
		function hide_error(){
		$("#mail_result").hide();
		}
    </script>
    <!--[if lt IE 8]>
       <div style=' clear: both; text-align:center; position: relative;'>
         <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
           <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
         </a>
      </div>
    <![endif]-->
    <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
    <![endif]-->
    </head>
    <body onLoad="document.form.submit();">
<!--==============================header=================================-->
<?php include("header.php"); ?>

<!--=======content================================-->

<section class="content">
      <div class="bg-1 v1"> </div>
      <div class="container_12">
    <div class="row">
          <div class="grid_12 manifestation ">
        <h2 class="v1"> Ultimate Clearing Session [UCS] Intense</h2>
        <div class="wrap_text">
              <ul class="one_li">
            <li>Facilitator </li>
            <li>Shiva Krishnan</li>
            <div class="clear"></div>
            <li>Mode</li>
            <li>Distance</li>
            <div class="clear"></div>
            <h3>Self Investment usd</h3>
            <div class="clear"></div>
            <li>Individual</li>
            <li>$135</li>
            <div class="clear"></div>
            <li>Family </li>
            <li>$297 [Family of four]</li>
            <div class="clear"></div>
            <li>Duration</li>
            <li>Two Hours</li>
          </ul>
              <p>This session is designed to remove all major energetic blocks that are influencing your life. It is a ‘must do’ for those who wish to take back control of their life. </p>
              <p>During the session we remove negative energies, etheric implants, de-activate parasites, activate DNA, clear all nine subtle bodies, clear each major organ, work on syncing the hemispheres of the brain and activation beyond 10% use, remove energetic imprints of existing patterns, remove negative thought pattern processes, and much more. </p>
              <p>A partial list of benefits include:</p>
              <ul class="teo_ul">
            <li>Remove the cause of all health issues.</li>
            <li> Heal the pain of broken hearts, betrayal, past abuse, ex-relationships, injustice, negative experiences, etc.</li>
            <li> Heal behavioural disorders. </li>
            <li> Clear negative patterns presently controlling your life.</li>
            <li> Increase spiritual awareness.</li>
            <li> Remove negative thought patterns, energies from yourself / family / home / business.</li>
            <li> Be freed of negative ancestral influences.</li>
            <li> Heal all past experiences in the present life.</li>
            <li> Heightened clarity of mind and increase brain function</li>
            <li>Free of candida and other bodily parasites.</li>
            <li> Be shielded from the effects of dark magic, dark entities, dark energies</li>
            <li> Protected from the negative influence of others.</li>
            <li> Become self aware and self empowered.</li>
            <li> Permanently remove the drain of life force from the body.</li>
            <li> Awaken all natural soul gifts.</li>
            <li>Receive a PERMANENT Golden Shield with your own personal boost ‘Activation code’.</li>
          </ul>
              <p>The total benefits are endless. Each session is tailored entirely to the individual, family, or business. </p>
              <p>Please contact Shiva Krishnan directly to confidentially discuss your situation</p>
              <div class="full_width_wrap"><a href="pay_Individual.html" class="pay_btn">Pay Individual</a> <a href="pay_family_session.html" class="pay_btn">Pay Family Session</a></div>
            </div>
        <div class="contact_frm">
              <div class="con_form">
            <h2>Contact For Further Enquiry</h2>
            <div id="mail_result"></div>
            <div class="full_formW">
                  <input type="text" name="first_name" id="first_name" onFocus="hide_error();" placeholder="First Name">
                </div>
            <div class="full_formW">
                  <input type="text" name="last_name" id="last_name" placeholder="Last Name">
                </div>
            <div class="full_formW">
                  <input type="text" name="email" id="email" onFocus="hide_error();" placeholder="Email" >
                </div>
            <div class="full_formW">
                  <textarea name="message" id="message" onFocus="hide_error();" placeholder="Message"></textarea>
                </div>
            <div class="full_formW">
                  <button onClick="mail_f();">Submit</button>
                </div>
          </div>
            </div>
      </div>
        </div>
  </div>
    </section>

<!--=======footer=================================-->

<?php include("footer.php"); ?>
</body>
</html>
