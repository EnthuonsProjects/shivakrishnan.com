<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="part_lft">
			<?php if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( 'Category Archives: %s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>

				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );
					endif;
				?>
			</header><!-- .archive-header -->
            
              <?php while ( have_posts() ) : the_post(); ?>
  <div class="blog_wrap">
    <h2 class="head"> <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a> </h2>
    <figure class="bl_img"> <a href="<?php echo get_permalink(); ?>">
      <?php the_post_thumbnail( ); ?>
      </a> </figure>
    <div class="blg_cnt">
      <ul class="blgAthr">
        <li><span class="blogicon1"> </span>
          <?php the_author_posts_link(); ?>
        </li>
        <li><span class="blogicon2"> </span><a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"> <?php echo get_the_date(); ?></a> </li>
        <li><span class="blogicon3"> </span> <a href="<?php echo get_permalink(); ?>">Replay</a></li>
      </ul>
      <div class="blog_des"> <?php echo wp_trim_words( get_the_content(), 48 ) ; ?> </div>
      <a class="bl_dis_a" href="<?php echo get_permalink(); ?>">Read More</a> </div>
  </div>
  <?php endwhile;  twentyfourteen_paging_nav();?>

			<?php
				
				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
		</div><!-- #content -->

<div class="part_ri8">
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
