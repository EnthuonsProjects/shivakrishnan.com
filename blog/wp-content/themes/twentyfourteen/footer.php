<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
</div>
</div>
</section>

<footer>
  <div class="container_12">
    <div class="row">
      <div class="grid_3 fright f-top"> <span class="f-text-1">Follow Us</span>
        <ul class="list-services clearfix">
          <li><a href="#"><i class="icon-google-plus-sign"></i></a></li>
          <li><a href="#"><i class="icon-twitter-sign"></i></a></li>
          <li><a href="#"><i class="icon-facebook-sign"></i></a></li>
          <li><a href="#"><i class="icon-rss-sign"></i></a></li>
        </ul>
      </div>
      <div class="grid_3 fright f-top box-3">
        <div><i class="icon-home"></i> <a href="mailto:shiva@shivakrishnan.com">shiva@shivakrishnan.com</a> <br>
          <a href="mailto:ruchika@shivakrishnan.com">ruchika@shivakrishnan.com</a></div>
        <div><i class="icon-phone"></i> Phone: +61 434 288 376</div>
      </div>
      <div class="grid_6 fright f-top">
        <ul class="list-1">
          <li><a href="<?php echo get_site_url(); ?>/../index.php">Home</a></li>
          <li><a href="<?php echo get_site_url(); ?>/../index-1.php">about us</a></li>
          <li><a href="<?php echo get_site_url(); ?>/index.php">Blog</a></li>
        </ul>
        <a class="logo" href="<?php echo get_site_url(); ?>/../index.php"><img src="<?php echo get_site_url(); ?>/../images/logo-footer.png" alt="shiva krishnan" title="shiva krishnan"></a>
        <div class="copyright"> <a href="<?php echo get_site_url(); ?>/../index.php" rel="nofollow">www.shivakrishnan.com </a></div>
      </div>
    </div>
  </div>
</footer>
<!-- #colophon -->

<?php wp_footer(); ?>
</body></html>