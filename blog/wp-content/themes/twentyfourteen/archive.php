<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="part_lft">
  <?php if ( have_posts() ) : ?>
  <header class="page-header">
    <h1 class="page-title">
      <?php
						if ( is_day() ) :
							printf( __( 'Daily Archives: %s', 'twentyfourteen' ), get_the_date() );

						elseif ( is_month() ) :
							printf( __( 'Monthly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'twentyfourteen' ) ) );

						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

						else :
							_e( 'Archives', 'twentyfourteen' );

						endif;
					?>
    </h1>
  </header>
  <!-- .page-header -->
  
  <?php while ( have_posts() ) : the_post(); ?>
  <div class="blog_wrap">
    <h2 class="head"> <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a> </h2>
    <figure class="bl_img"> <a href="<?php echo get_permalink(); ?>">
      <?php the_post_thumbnail( ); ?>
      </a> </figure>
    <div class="blg_cnt">
      <ul class="blgAthr">
        <li><span class="blogicon1"> </span>
          <?php the_author_posts_link(); ?>
        </li>
        <li><span class="blogicon2"> </span><a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"> <?php echo get_the_date(); ?></a> </li>
        <li><span class="blogicon3"> </span> <a href="<?php echo get_permalink(); ?>">Replay</a></li>
      </ul>
      <div class="blog_des"> <?php echo wp_trim_words( get_the_content(), 48 ) ; ?> </div>
      <a class="bl_dis_a" href="<?php echo get_permalink(); ?>">Read More</a> </div>
  </div>
  <?php endwhile;  twentyfourteen_paging_nav();?>
  <?php
				
				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
</div>
<!-- #content -->
<div class="part_ri8">
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
