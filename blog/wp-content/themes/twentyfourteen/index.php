<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>

<div class="part_lft">
  <?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
  <div class="blog_wrap">
    <h2 class="head"> <a href="<?php echo get_permalink(); ?>"><?php echo get_the_title(); ?></a> </h2>
    <figure class="bl_img"> <a href="<?php echo get_permalink(); ?>">
      <?php the_post_thumbnail( ); ?>
      </a> </figure>
    <div class="blg_cnt">
      <ul class="blgAthr">
        <li><span class="blogicon1"> </span>
          <?php the_author_posts_link(); ?>
        </li>
        <li><span class="blogicon2"> </span><a href="<?php echo get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d')); ?>"> <?php echo get_the_date(); ?></a> </li>
        <li><span class="blogicon3"> </span> <a href="<?php echo get_permalink(); ?>">Replay</a></li>
      </ul>
      <div class="blog_des"> <?php echo wp_trim_words( get_the_content(), 48 ) ; ?> </div>
      <a class="bl_dis_a" href="<?php echo get_permalink(); ?>">Read More</a> </div>
  </div>
  <?php endwhile;  twentyfourteen_paging_nav();?>
  <?php else :  get_template_part( 'content', 'none' ); ?>
  <?php endif; ?>
</div>
<div class="part_ri8">
  <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>
