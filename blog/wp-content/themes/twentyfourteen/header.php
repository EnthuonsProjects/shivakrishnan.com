<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/../css/camera.css">
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/../css/style.css">
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/../css/superfish.css">
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/../font/font-awesome.css" type="text/css" media="screen">
<link rel="stylesheet" href="<?php echo get_site_url(); ?>/../css/custom.css">
<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php if ( get_header_image() ) : ?>
<div id="site-header"> <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"> <img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt=""> </a> </div>
<?php endif; ?>
<header>
  <div class="container_12">
    <div class="row">
  <div class="grid_12 clearfix">
    <div class="clearfix fleft sn_logo">
      <h1><a href="<?php echo get_site_url(); ?>/../index.php"><img src="<?php echo get_site_url(); ?>/../images/logo.png" alt="shiva krishnan" title="shiva krishnan" /></a></h1>
      <div class="tagLine">‘Manifestation is not about creating an item or a situation in your life. It’s entirely about becoming the vibration at which what you desire already exists.’
        <div class="right_go"> - Shiva Krishnan </div>
      </div>
    </div>
    <nav class="clearfix">
      <form id="search1" class="search" action="<?php bloginfo('url'); ?>/" method="GET">
            <div>
              <input type="text" name="s">
              <a onClick="document.getElementById('search1').submit()" class="button1"><i class="icon-search"></i></a> </div>
          </form>
      <ul class="sf-menu clearfix">
        <li><a href="<?php echo get_site_url(); ?>/../index.php">Home</a></li>
        <li><a href="<?php echo get_site_url(); ?>/../index-1.php">About us</a></li>
        <li class="current"><a href="<?php echo get_site_url(); ?>/index.php">Blog</a></li>
      </ul>
    </nav>
  </div>
</div>

  </div>
</header>
<!-- #masthead -->
<section class="content mainWrap">
<div class="bg-1 v1 mainWrap"> </div>
<div class="inner_wrap">
<div class="wrap_inner">
